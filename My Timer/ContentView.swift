//
//  ContentView.swift
//  My Timer
//
//  Created by M Ramdhan Syahputra on 14/03/24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HomeView()
    }
}

struct HomeView: View {
    @State private var start = false
    @State private var to: CGFloat = 0
    @State private var time = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    @State private var count = 0
    @State private var end = 20
    @State private var isEdited = false
    
    var body: some View {
        NavigationStack {
            VStack(spacing: 64) {
                ZStack {
                    Circle()
                        .trim(from: 0, to: 1)
                        .stroke(.black.opacity(0.09), style: StrokeStyle(lineWidth: 35, lineCap: .round))
                        .frame(width: 240, height: 240)
                    
                    Circle()
                        .trim(from: 0, to: to)
                        .stroke(.blue, style: StrokeStyle(lineWidth: 35, lineCap: .round))
                        .frame(width: 240, height: 240)
                        .rotationEffect(.degrees(-90))
                    
                    VStack(spacing: 12) {
                        Text(String(count))
                            .font(.system(size: 64))
                            .bold()
                        
                        Text("Of \(end)")
                    }
                }
                
                HStack(spacing: 32) {
                    Button {
                        start.toggle()
                    } label: {
                        Label(
                            title: { Text(start ? "Pause" : "Start") },
                            icon: { Image(systemName: start ? "pause" : "play") }
                        )
                            .foregroundStyle(.white)
                            .padding()
                            .frame(width: (UIScreen.main.bounds.width / 2) - 55)
                            .background(.blue)
                            .clipShape(.capsule)
                    }
                    
                    Button {
                        withAnimation {
                            start = false
                            count = 0
                            to = 0
                        }
                    } label: {
                        Label(
                            title: { Text("Restart") },
                            icon: { Image(systemName: "gobackward") }
                        )
                            .padding()
                            .frame(width: (UIScreen.main.bounds.width / 2) - 55)
                            .background(
                                Capsule()
                                    .stroke(.blue, lineWidth: 2)
                            )
                    }
                }
                
                Button {
                    isEdited.toggle()
                } label: {
                    Label(
                        title: { Text("Edit time") },
                        icon: { Image(systemName: "square.and.pencil") }
                    )
                        .foregroundStyle(.white)
                        .padding()
                        .frame(width: (UIScreen.main.bounds.width / 2) - 55)
                        .background(.orange)
                        .clipShape(.capsule)
                }
            }
            .sheet(isPresented: $isEdited, content: {
                VStack(spacing: 8) {
                    Text("Change end of timer")
                        .font(.title)
                    
                    Picker("End of timer", selection: $end) {
                        ForEach(10...100, id: \.self) { data in
                            Text(String(data))
                        }
                    }
                    .pickerStyle(.wheel)
                }
                .presentationDetents([.height(300)])
            })
            .navigationTitle("My Timer")
        }
        .onReceive(time, perform: { _ in
            debugPrint("Tick: \(count)")
            
            if start {
                if count == end {
                    withAnimation {
                        count = 0
                        to = 0
                        start.toggle()
                    }
                    notify()
                    return
                }
                
                withAnimation {
                    count += 1
                    to += CGFloat(count) / (CGFloat(end) * 10)
                }
                return
            }
            
            
        })
        .onAppear {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound, .badge]) { _, _ in }
        }
    }
    
    func notify() {
        // Configure the notification's payload.
        let content = UNMutableNotificationContent()
        content.title = "My Timer"
        content.body = "Yuhuuu!! timer has finished"
        content.sound = UNNotificationSound.default
         
        // Deliver the notification in five seconds.
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        let request = UNNotificationRequest(identifier: "Notify", content: content, trigger: trigger) // Schedule the notification.
        
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error : Error?) in
             if let theError = error {
                 // Handle any errors
             }
        }
    }
}

#Preview {
    ContentView()
}
