//
//  My_TimerApp.swift
//  My Timer
//
//  Created by M Ramdhan Syahputra on 14/03/24.
//

import SwiftUI

@main
struct My_TimerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
